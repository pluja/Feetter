<p align="center"><img width="200" src="misc/logo.png"></p>
<p align="center">Create and manage anonymous feeds in <b>Nitter</b> with a simple, fast web interface.</p>
<p align="center"><a href="https://codeberg.org/pluja">Check out my other privacy projects</a></p>

- [What is this](#what-is-this)
- [Features](#features)
- [Install](#install)
  - [Local run](#local-run)
  - [Self-hosting](#self-hosting)
- [Roadmap](#roadmap)
- [Screenshots](#screenshots)
- [Built with](#built-with)
- [Public Instances](#public-instances)

## What is this

Feetter allows you to create Twitter feeds using the alternative private frontend Nitter. You only need a randomly generated username to use Feetter. No account and no personal data is required. 

You can create multiple feeds up to 30 users, it uses the Nitter "multifeed" option to create your feeds. Feetter is just a nice way to manage these feeds and have them synced in any device you want.

## Features
- [x] Create and manage Nitter feeds.
- [x] Save tweets via URL and display them
- [x] Directly visit your Nitter feeds.
- [x] Create as many feeds as you like.
- [x] Lightweight and fast.
- [x] No JS, No Cookies, No tracking.
- [x] No registration: just a randomly generated username.
- [x] Multiplatform, syncs everywhere.
- [x] Optimized for small screens. 

## Install

### Local run (dev)
1. Clone this repo.
2. Install python3 and python dependencies from `requirements.txt`
3. `mkdir data`
3. Run `sanic feetter.app`
4. Visit http://127.0.0.1:8000

### Self-hosting
#### With Docker (production)
> This run uses Gunicorn, so it is instance-ready.
> Docker commands may require to use `sudo`

1. Install docker.
2. Create the data directory (e.g. `mkdir -p /path/to/feetter/data`)
2. Pull the image: `docker pull pluja/feetter`
3. Run the container:
  - `docker run -p 1333:1111 -v /path/to/feetter/data:/app/data --name feetter -d pluja/feetter`
4. Visit `http://localhost:1337/` and enjoy :)

#### Update
- Run `docker pull pluja/feetter`
- Run `docker stop feetter`
- Run `docker rm feetter`
- Run `docker run -p 1333:1111 -v /path/to/feetter/data:/app/data --name feetter -d pluja/feetter`

## Roadmap

- [x] Delete a feed
- [x] Edit a feed: Add users
- [x] Edit a feed: Remove users
- [x] Optimize for small screens.
- [x] Export data as json file
- [ ] Edit a feed: Change name
- [ ] Import data from json file
- [ ] Autodelete empty users
- [ ] Display feed tweets in app from rss (less data)


## Built with

![Python](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)
![HTML](https://img.shields.io/badge/HTML-239120?style=for-the-badge&logo=html5&logoColor=white)
![Sanic](https://img.shields.io/badge/-SANIC-ff69b4?style=for-the-badge)
![Jinja2](https://img.shields.io/badge/-Jinja2-B41717?style=for-the-badge&logo=jinja)


## [Support](https://kycnot.me/about#support)
### Public Instances
| Instance	| Server	|
|-	|-	|
| [feetter.r3d.red](https://feetter.r3d.red) 	| 🇩🇪 	|
