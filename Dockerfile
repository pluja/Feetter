FROM python:3.10-slim AS base
# Image to Build Dependencies
FROM base AS builder

WORKDIR /app

COPY ./requirements.txt /app

# Build Dependencies

# Python Dependencies
#RUN pip install --no-cache-dir --prefix=/install gunicorn
RUN pip install --no-cache-dir --prefix=/install -r requirements.txt

# Runtime Environment Image
FROM base


WORKDIR /app
# Runtime Dependencies
#RUN apk --no-cache add libxml2 libxslt
COPY --from=builder /install /usr/local
COPY . /app/

#RUN mkdir /app/data
EXPOSE 1111
CMD sanic feetter.app --host=0.0.0.0 --port=1111 --workers=4
